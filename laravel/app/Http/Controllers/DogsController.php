<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Dogs;

class DogsController extends Controller
{
    public function index()
    {
    	$dogs = Dogs::latest()->get();
    	return view('iqbal.index',compact('dogs'));
    }

    public function show($id)
    {
    	$dog= Dogs::find($id);
    	return view('iqbal.show',compact('dog'));
    }
}
