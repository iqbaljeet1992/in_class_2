@extends('iqbal.layout')

@section('content')

<h1>List of Dogs</h1>

	<ul>
	@foreach($dogs as $dog)
		
		<li><a href="/dogs/{{$dog['id']}}">{{$dog['name']}}</a></li>

	@endforeach
	</ul>

@endsection